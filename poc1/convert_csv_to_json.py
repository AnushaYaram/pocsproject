import csv
import json


def read_csv():
    """

    :return: it returns list of  data from csv file
    """
    rows = []
    with open('file1.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader)
        for row in csv_reader:
            rows.append(row)
        return rows


def convert_json(rows):
    """

    :param rows: a list of data it will convert to json format
    :return: it will return json object
    """
    json_format_csv = []
    for row in rows:
        dict_row = {
            "Name": row[0],
            "Branch": row[1],
            "Year": row[2],
            "CGPA": row[3]
        }
        json_dict_row = json.dumps(dict_row, indent=4)
        json_format_csv.append(json_dict_row)
    return json_format_csv


def output_json():
    """

    :return: the json output will be stored in an another file
    """
    processed_data = convert_json(read_csv())
    with open("output_json", 'w')as f:
        for row in processed_data:
            f.write(row)
    print(processed_data)


if __name__ == "__main__":
    output_json()
