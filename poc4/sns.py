

from poc1.convert_csv_to_json import output_json
from aws_client import sns


response = sns.create_topic(Name="topic_project4")
topic_arn = response["TopicArn"]
response = sns.subscribe(TopicArn=topic_arn, Protocol="email", Endpoint="anushareddyyaram34@gmail.com")
subscription_arn = response["SubscriptionArn"]


def email():
    sns.publish(TopicArn=topic_arn,
                Message=output_json(),
                Subject="Welcome to aws simple notification services  ")


if "__name__" == "__main__":
    email()
